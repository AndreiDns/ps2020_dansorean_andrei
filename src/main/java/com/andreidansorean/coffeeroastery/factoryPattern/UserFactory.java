package com.andreidansorean.coffeeroastery.factoryPattern;

import com.andreidansorean.coffeeroastery.entities.Admin;
import com.andreidansorean.coffeeroastery.entities.Customer;
import com.andreidansorean.coffeeroastery.entities.User;

public class UserFactory {

    /**
     *
     * @param usertype admin or customer
     * @return new User, either as Admin or as Customer, based on usertype
     */
    public static User createUser(String usertype)
    {
        if(usertype.equals("ADMIN")){
            return new Admin();
        }
        else if(usertype.equals("CUSTOMER")){
            return new Customer();
        }
        else return null;

    }
}
