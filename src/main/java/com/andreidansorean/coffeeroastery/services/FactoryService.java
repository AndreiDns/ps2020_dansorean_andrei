/*package com.andreidansorean.coffeeroastery.services;

import com.andreidansorean.coffeeroastery.entities.User;
import com.andreidansorean.coffeeroastery.factoryPattern.SpringFactoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class FactoryService {

    @Autowired
    private List<User> users;

    private final Map<String, User> userCache = new HashMap<>();

    @PostConstruct
    public void initCache(){
        for(User user : users){
            userCache.put(user.getUserType(), user);
        }
    }

    /**
     * Factory method to get the user instance depending upon algorithm
     * type.
     *
     * @param userType
     *            the algorithm type
     * @return user


        public User getUser(String userType){
            User user = userCache.get(userType);

            return user;
        }

}
*/