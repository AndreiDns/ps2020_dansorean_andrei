package com.andreidansorean.coffeeroastery.repositories;

import com.andreidansorean.coffeeroastery.entities.Coffee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoffeeRepository extends JpaRepository<Coffee, Integer> {
}
