package com.andreidansorean.coffeeroastery.controllers;

import com.andreidansorean.coffeeroastery.entities.Coffee;
import com.andreidansorean.coffeeroastery.entities.Order;
import com.andreidansorean.coffeeroastery.entities.User;
import com.andreidansorean.coffeeroastery.services.OrderService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

//import static com.sun.javaws.JnlpxArgs.verify;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
class OrderControllerTest {

    @Mock
    private OrderService orderService;

    @InjectMocks
    private OrderController orderController;

    @Rule
    MockitoRule mockitoRule = MockitoJUnit.rule();


    @Test
    void getAll() {

        List<Order> expectedList = new ArrayList<>();
        User u1 = new User();
        u1.setId(1);

        Coffee c1 = new Coffee();
        c1.setCoffeeId(2);

        Order o1 = new Order(1,2,"13.03.2020", 0);
        expectedList.add(o1);

        when(orderService.getByUserId(1)).thenReturn(expectedList);

        List<Order> currentList = orderService.getByUserId(1);

        assertEquals(expectedList, currentList);
    }

    @Test
    void add() {
        User u1 = new User();
        u1.setId(1);

        Coffee c1 = new Coffee();
        c1.setCoffeeId(2);

        Order o1 = new Order(1,2,"13.03.2020", 0);

        when(orderService.add(o1)).thenReturn(o1);

        assertEquals(orderService.add(o1), o1);
        verify(orderService).add(o1);
    }
}