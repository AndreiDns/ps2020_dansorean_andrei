package com.andreidansorean.coffeeroastery.controllers;


import com.andreidansorean.coffeeroastery.entities.Coffee;
import com.andreidansorean.coffeeroastery.services.CoffeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Controller used for CRUD operations on User table "coffee" in database.
 */

@RestController
@RequestMapping("/coffee") //localhost:8080/
public class CoffeeController {

    @Autowired
    CoffeeService coffeeService;

    @GetMapping("/verifyObserver")
    public String observerWorks(){return coffeeService.tryObserver();}


    /**
     *
     * @param id the id to look up in the list of Coffee objects
     * @return Coffee object with corresponding id
     */
    @GetMapping ("/get/{id}")
    public Coffee getById(@PathVariable(name = "id") Integer id) { return coffeeService.getById(id); }

    /**
     *
     * @return List of all coffee objects in DB
     */
    @GetMapping("/getAll")
    public List<Coffee> getAll(){ return coffeeService.getAll(); }

    /**
     *
     * @param coffee object to be added in DB
     * @return object details
     */
    @PostMapping("/insert")
    public Coffee add(@RequestBody Coffee coffee){ return coffeeService.addCoffee(coffee); }

    /**
     *
     * @param coffee object to update an existing coffee object in DB
     * @return coffee Object with changes made
     */
    @PutMapping("/update")
    public Coffee update(@RequestBody Coffee coffee){ return coffeeService.updateCoffee(coffee); }

    /**
     *
     * @param id delete coffee object at corresponding id
     */
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable(name = "id") Integer id){ coffeeService.deleteCoffee(id); }
}
