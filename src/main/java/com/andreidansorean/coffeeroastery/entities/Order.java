package com.andreidansorean.coffeeroastery.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@Table(name = "orders")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_order")
    private Integer idOrder;

    @Column(name = "id_user")
    private Integer idUser;

    @Column(name = "id_coffee")
    private Integer idCoffee;

    @Column(name = "checkout_date")
    private String checkOutDate;

    @Column(name = "quantity")
    private Integer quantity;



    public Order() {
    }

    public Order(Integer idUser, Integer idCoffee, String checkOutDate, Integer quantity) {
        this.idUser = idUser;
        this.idCoffee = idCoffee;
        this.checkOutDate = checkOutDate;
        this.quantity = quantity;
    }

    public Integer getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Integer idOrder) {
        this.idOrder = idOrder;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser(Integer idUser) {
        this.idUser = idUser;
    }

    public Integer getIdCoffee() {
        return idCoffee;
    }

    public void setIdCoffee(Integer idCoffee) {
        this.idCoffee = idCoffee;
    }

    public String getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

}
