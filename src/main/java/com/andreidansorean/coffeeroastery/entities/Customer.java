package com.andreidansorean.coffeeroastery.entities;

public class Customer extends User{

    public Customer(){}

    public Customer(String username, String password, String email, String userType) {
        super(username, password, email, userType);
    }

    public String saySomething(){return "Customer created";}
}
