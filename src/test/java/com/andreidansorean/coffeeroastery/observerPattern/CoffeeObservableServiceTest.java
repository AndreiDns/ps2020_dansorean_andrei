package com.andreidansorean.coffeeroastery.observerPattern;

import com.andreidansorean.coffeeroastery.entities.Coffee;
import com.andreidansorean.coffeeroastery.entities.User;
import com.andreidansorean.coffeeroastery.services.UserService;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class CoffeeObservableServiceTest {

    @Mock
    IObserver iobserver;

    @InjectMocks
    CoffeeObservableService coffeeObservableService;

    @Rule
    MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    void subscribe() {

    }

    @Test
    void unsubscribe() {
    }

    @Test
    void reportChanges() {
    }
}