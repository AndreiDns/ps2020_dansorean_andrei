package com.andreidansorean.coffeeroastery.controllers;


import com.andreidansorean.coffeeroastery.entities.User;
import com.andreidansorean.coffeeroastery.factoryPattern.UserFactory;
import com.andreidansorean.coffeeroastery.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Controller used for CRUD operations on User table in "pstema1" database.
 */
@RestController
@RequestMapping("/pstema1")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/verifyObserver") // localhost:8080/
    public String getHelloWorld() {
        return userService.notifyObservers();
    }

    /**
     *
     * @param id the id to look up in the list of User objects
     * @return User object with corresponding id
     */
    @GetMapping ("/get/{id}")
    public User getById(@PathVariable(name = "id") Integer id) { return userService.getById(id); }


    /**
     *
     * @return List of all users in DB
     */
    @GetMapping ("/getAll")
    public List<User> getAll() { return userService.getAll(); }

    /**
     *
     * @param user to be inserted in DB, must be JSON format
     */
    @PostMapping("/insert")
    public User insert (@RequestBody User user) { return userService.add(user); }

    /**
     *
     * @param user to be updated in DB, must be JSON format
     * @return
     */
    @PutMapping("/update")
    public User update(@RequestBody User user) { return userService.updateUser(user); }

    /**
     *
     * @param id is passed to the delete the user with the corresponding id
     */
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable(name = "id") int id){ userService.delete(id); }
}
