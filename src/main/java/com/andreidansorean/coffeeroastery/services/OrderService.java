package com.andreidansorean.coffeeroastery.services;

import com.andreidansorean.coffeeroastery.entities.Order;
import com.andreidansorean.coffeeroastery.repositories.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    @Autowired
    private OrderRepository orderRepository;

    public List<Order> getAll(){ return orderRepository.findAll(); }

    public List<Order> getByUserId(Integer id){ return orderRepository.findByIdUser(id); }

    public Order add(Order order) { return orderRepository.save(order); }
}
