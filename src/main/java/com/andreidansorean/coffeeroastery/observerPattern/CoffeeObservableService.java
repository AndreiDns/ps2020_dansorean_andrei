package com.andreidansorean.coffeeroastery.observerPattern;

import com.andreidansorean.coffeeroastery.entities.Coffee;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


public class CoffeeObservableService {

    private List<IObserver> subscribers;

    public CoffeeObservableService(){ subscribers = new ArrayList<>(); }

    /**
     *
     * @param subscriber adds a subscriber to the subscribers list
     */
    public void subscribe(IObserver subscriber) {subscribers.add(subscriber);}

    /**
     *
     * @param subscriber removes from subscribers list
     */
    public void unsubscribe (IObserver subscriber){subscribers.remove(subscriber);}


    /**
     * function sents notifications to all Observers subscribed to curent Observable.
     * @return string confirming all notifications have been sent
     */
    public String reportChanges(){

        for(IObserver subscriber : subscribers){
            subscriber.notifyObservers();
        }
        return "Sent notification to all Observers";

    }
}
