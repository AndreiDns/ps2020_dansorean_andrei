package com.andreidansorean.coffeeroastery.observerPattern;

import com.andreidansorean.coffeeroastery.entities.Coffee;

import java.util.List;

public interface IObserver {

    public String notifyObservers();
}
