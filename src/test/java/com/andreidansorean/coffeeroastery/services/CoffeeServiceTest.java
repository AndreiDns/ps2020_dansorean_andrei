package com.andreidansorean.coffeeroastery.services;

import com.andreidansorean.coffeeroastery.entities.Coffee;
import com.andreidansorean.coffeeroastery.repositories.CoffeeRepository;
import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


/**
 * Coffee Service class communicates with the JPA UserRepository interface for CRUD operations.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
class CoffeeServiceTest {

    @Mock
    CoffeeRepository coffeeRepository;

    @InjectMocks
    CoffeeService coffeeService;

    @Rule
    MockitoRule mockitoRule = MockitoJUnit.rule();

    @Test
    public void testMocking(){
        coffeeService.getAll();
        verify(coffeeRepository).findAll();
    }

    @Test
    void getAll() {
        List<Coffee> expectedList = new ArrayList<>();

        Coffee c1 = new Coffee();
        Coffee c2 = new Coffee();

        expectedList.add(c1);
        expectedList.add(c2);

        when(coffeeRepository.findAll()).thenReturn(expectedList);

        List<Coffee> currentList = coffeeService.getAll();

        assertEquals(expectedList.size(), currentList.size());
        verify(coffeeRepository).findAll();
    }

    @Test
    void addCoffee() {
        Coffee c1 = new Coffee("Tulip", "HardRoasted", 21.0, 4);
        when(coffeeRepository.save(c1)).thenReturn(c1);
        assertEquals(c1, coffeeService.addCoffee(c1));
        verify(coffeeRepository).save(c1);
    }

    @Test
    void updateCoffee() {
        Coffee c1 = new Coffee("Olivo", "HardRoasted", 21.0, 4);

        when(coffeeRepository.saveAndFlush(c1)).thenReturn(c1);
        assertEquals(c1, coffeeService.updateCoffee(c1));
    }

    @Test
    void deleteCoffee() {
        Coffee c1 = new Coffee("Olivo", "HardRoasted", 21.0, 4);
        c1.setCoffeeId(10);
        coffeeService.deleteCoffee(10);
        verify(coffeeRepository).deleteById(10);
    }
}