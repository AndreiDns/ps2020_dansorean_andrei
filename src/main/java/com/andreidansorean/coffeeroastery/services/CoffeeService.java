package com.andreidansorean.coffeeroastery.services;


import com.andreidansorean.coffeeroastery.entities.Coffee;
import com.andreidansorean.coffeeroastery.observerPattern.CoffeeObservableService;
import com.andreidansorean.coffeeroastery.repositories.CoffeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CoffeeService extends CoffeeObservableService{

    @Autowired
    CoffeeRepository coffeeRepository;

    @Autowired
    CoffeeObservableService coffeeObservableService;

    /**
     *
     * @return A string if all calls for Observers are made.
     */
    public String tryObserver(){ return coffeeObservableService.reportChanges();}

    /**
     *
     * @param id used for searching the user with corresponding id
     * @return coffee object that has the corresponding id
     */
    public Coffee getById(Integer id) {return coffeeRepository.getOne(id);}

    /**
     *
     * @return List of all coffee objects in DB
     */
    public List<Coffee> getAll() { return coffeeRepository.findAll(); }

    /**
     *
     * @param coffee object to add in DB
     * @return object added in DB
     */
    public Coffee addCoffee(Coffee coffee) { return coffeeRepository.save(coffee); }

    /**
     *
     * @param coffee object to update another coffee object in DB
     * @return changed object in DB
     */
    public Coffee updateCoffee(Coffee coffee){
        //tryObserver();
        return coffeeRepository.saveAndFlush(coffee);
    }

    /**
     *
     * @param id used for deleting coffee object corresponding to id
     */
    public void deleteCoffee(Integer id){ coffeeRepository.deleteById(id); }

}
