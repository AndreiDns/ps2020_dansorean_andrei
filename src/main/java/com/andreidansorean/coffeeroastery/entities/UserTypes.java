package com.andreidansorean.coffeeroastery.entities;

public enum UserTypes {
    ADMIN,
    CONSUMER
}
