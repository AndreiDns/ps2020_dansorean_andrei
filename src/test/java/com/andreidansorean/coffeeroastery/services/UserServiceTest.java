package com.andreidansorean.coffeeroastery.controllers;

import com.andreidansorean.coffeeroastery.entities.User;
import com.andreidansorean.coffeeroastery.observerPattern.CoffeeObservableService;
import com.andreidansorean.coffeeroastery.repositories.UserRepository;
import com.andreidansorean.coffeeroastery.services.UserService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
class UserServiceTest {

    @Mock
    private UserRepository userRepository;


    @InjectMocks
    private UserService userService = new UserService();


    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    private User user;

    @Before
    void setUp() throws Exception{
    }

    @Test
    public void testMocking()
    {
        userService.getAll();
        verify(userRepository).findAll() ;
    }


    @Test
    public void getById(){
        List<User> expectedList = new ArrayList<>();
        User u1 = new User("Andrei", "1234", "gmail", "ADMIN");
        User u2 = new User("Vasile", "12345", "yahoo", "CUSTOMER");

        u1.setId(1);
        u2.setId(2);

        expectedList.add(u1);
        expectedList.add(u2);

        when(userRepository.getOne(1)).thenReturn(u1);
        assertEquals(u1, userService.getById(1));
    }

    @Test
    public void getAll() {
        List<User> expectedList = new ArrayList<>();
        User u1 = new User("Andrei", "1234", "gmail", "ADMIN");
        User u2 = new User("Vasile", "12345", "yahoo","CUSTOMER");
        expectedList.add(u1);
        expectedList.add(u2);
        when(userRepository.findAll()).thenReturn(expectedList);
        List<User> currentList = userService.getAll();
        assertEquals(expectedList, currentList);
    }

    @Test
    void insert() {
        User user = new User("Pali", "0000", "hotmail", "CUSTOMER");
        when(userRepository.save(user)).thenReturn(user);
        assertEquals(user, userService.add(user));

    }

    @Test
    void update() {
        User user = new User("Pali", "0000", "hotmail", "CUSTOMER");
        user.setUsername("Valory");
        when(userRepository.saveAndFlush(user)).thenReturn(user);
        assertEquals(user, userService.updateUser(user));
    }

    @Test
    void delete() {
        User user = new User("Pali", "0000", "hotmail", "CUSTOMER");
        user.setId(10);
        userService.delete(10);
        verify(userRepository).deleteById(10);
    }
}