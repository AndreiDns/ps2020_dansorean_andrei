package com.andreidansorean.coffeeroastery;

import com.andreidansorean.coffeeroastery.observerPattern.CoffeeObservableService;
import com.andreidansorean.coffeeroastery.services.CoffeeService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoffeeRoasteryApplication {

    public static void main(String[] args) {

        SpringApplication.run(CoffeeRoasteryApplication.class, args);
    }

}
