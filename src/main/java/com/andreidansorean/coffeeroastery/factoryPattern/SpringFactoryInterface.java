package com.andreidansorean.coffeeroastery.factoryPattern;

public interface SpringFactoryInterface {

    /**
     * Method to get the Decryption Algorithm type.
     *
     * @return String
     */
    String createUser();

    String getUserType();

    String saySomething(String string);
}
