package com.andreidansorean.coffeeroastery.repositories;

import com.andreidansorean.coffeeroastery.entities.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface OrderRepository extends JpaRepository<Order, Integer> {
    List<Order> findByIdUser(Integer user_id);
}
