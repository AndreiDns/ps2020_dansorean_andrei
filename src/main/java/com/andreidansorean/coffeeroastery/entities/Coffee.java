package com.andreidansorean.coffeeroastery.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@Table(name = "coffee")
public class Coffee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_coffee")
    private Integer coffeeId;

    @Column(name = "coffee_name")
    private String coffeeName;

    @Column(name = "coffee_type")
    private String coffeeType;

    @Column(name = "price")
    private Double price;

    @Column(name = "quantity")
    private Integer quantity;

   // private ArrayList<Observer> observers;

    public Coffee(){
        //observers = new ArrayList<Observer>();
    }

    public Coffee(String coffeeName, String coffeeType, Double price, Integer quantity) {
        this.coffeeName = coffeeName;
        this.coffeeType = coffeeType;
        this.price = price;
        this.quantity = quantity;
    }

    public Integer getCoffeeId() {
        return coffeeId;
    }

    public void setCoffeeId(Integer coffeeId) {
        this.coffeeId = coffeeId;
    }

    public String getCoffeeName() {
        return coffeeName;
    }

    public void setCoffeeName(String coffeeName) {
        this.coffeeName = coffeeName;
    }

    public String getCoffeeType() {
        return coffeeType;
    }

    public void setCoffeeType(String coffeeType) {
        this.coffeeType = coffeeType;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {

        this.price = price;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {

        this.quantity = quantity;
       // notifyObserver();
    }

}
