package com.andreidansorean.coffeeroastery.services;


import com.andreidansorean.coffeeroastery.entities.Coffee;
import com.andreidansorean.coffeeroastery.entities.User;
import com.andreidansorean.coffeeroastery.observerPattern.CoffeeObservableService;
import com.andreidansorean.coffeeroastery.observerPattern.IObserver;
import com.andreidansorean.coffeeroastery.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * User service class communicates with the JPA UserRepository interface for CRUD operations.
 */
@Service
public class UserService implements IObserver {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private CoffeeObservableService coffeeObservable;

    @Autowired
    public UserService(){

    }
   // @Autowired
    public UserService(CoffeeObservableService coffeeObservable){
        this.coffeeObservable = coffeeObservable;
        this.coffeeObservable.subscribe(this);
    }

    @Override
    public String notifyObservers(){
        System.out.println("In notifyObservers function");
    return "Observer Works";
    }


    /**
     *
     * @param id used for searching the user with corresponding id
     * @return user that has the corresponding id
     */
    public User getById(Integer id) { return userRepository.getOne(id);}

    /**
     *
     * @return List for all users in DB
     */
    public List<User> getAll(){ return userRepository.findAll(); }

    /**
     *
     * @param user object passed to be saved in DB
     * @return added user
     */
    public User add(User user) { return userRepository.save(user); }

    /**
     *
     * @param user to be updated
     * @return updated information in the user object
     */
    public User updateUser(User user){ return userRepository.saveAndFlush(user); }

    /**
     *
     * @param id to delete user with corresponding id
     */
    public void delete(Integer id) { userRepository.deleteById(id); }
}
