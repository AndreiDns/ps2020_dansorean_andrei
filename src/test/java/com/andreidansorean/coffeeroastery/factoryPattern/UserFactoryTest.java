package com.andreidansorean.coffeeroastery.factoryPattern;

import com.andreidansorean.coffeeroastery.entities.Admin;
import com.andreidansorean.coffeeroastery.entities.Customer;
import com.andreidansorean.coffeeroastery.entities.User;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserFactoryTest {

    @Test
    void createUser1() {
        assertTrue(UserFactory.createUser("ADMIN") instanceof Admin);
    }

    @Test
    void createUser2() {
        assertTrue(UserFactory.createUser("CUSTOMER") instanceof Customer);
    }

}