package com.andreidansorean.coffeeroastery.controllers;


import com.andreidansorean.coffeeroastery.entities.Coffee;
import com.andreidansorean.coffeeroastery.entities.Order;
import com.andreidansorean.coffeeroastery.entities.User;
import com.andreidansorean.coffeeroastery.services.CoffeeService;
import com.andreidansorean.coffeeroastery.services.OrderService;
import com.andreidansorean.coffeeroastery.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/orders")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private CoffeeService coffeeService;

    @Autowired
    private UserService userService;

    @GetMapping("/orderWorks")
    public String saySomething(){ return "order mapping works"; }

    /**
     *
     * @param id is used to find user with respective id
     * @return returns Order object that has the specified User id
     */
    @GetMapping("/{id}")
    public List<Order> getAll(@PathVariable(name = "id") Integer id){
        User user = userService.getById(id);
        if(user.getUserType().equals("ADMIN")){
            return orderService.getAll();
        }
        else return orderService.getByUserId(id);
    }

    /**
     *
     * @param order object is sent to be added in the Order List
     * @return returns new created order using HttpStatus
     */
    @PostMapping("/add")
    public ResponseEntity<?> add(@Valid @RequestBody Order order){
        Coffee c = coffeeService.getById(order.getIdCoffee());
        if(c.getQuantity() > 1){
            Order o = orderService.add(order);
            c.setQuantity(c.getQuantity()-1);
            coffeeService.reportChanges();
            coffeeService.updateCoffee(c);
            return new ResponseEntity<Order>(o, HttpStatus.CREATED);
        }
        return null;
    }
}
