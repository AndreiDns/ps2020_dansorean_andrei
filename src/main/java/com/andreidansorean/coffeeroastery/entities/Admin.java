package com.andreidansorean.coffeeroastery.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.Entity;
import javax.persistence.Table;

public class Admin extends User {

    public Admin(){}

    public Admin(String username, String password, String email, String userType) {
        super(username, password, email, userType);
    }

    public String saySomething(){return "Admin created";}
}
